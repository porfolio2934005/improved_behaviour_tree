namespace BehaviourTree
{
    public interface IQuicRecursiveSort : QuicSort
    {
        void Sort(Node[] array, int low, int high);
        
        void OrderNodes();
    }
}