namespace BehaviourTree
{
    public  class SimpleSelector : Selector, ISelector
    {
        public SimpleSelector(string n) : base(n)
        {
            name = n;
        }

        public override Status Process()
        {
            Status childstatus = children[currentChild].Process();
            if (childstatus == Status.RUNNING) return Status.RUNNING;

            if (childstatus == Status.SUCCESS)
            {
                currentChild = 0;
                ReturnSuccessSelector();
            }

            currentChild++;
            if (currentChild >= children.Count)
            {
                currentChild = 0;
                ReturnFailureSelector();
            }

            return Status.RUNNING;
        }

        public Status ReturnSuccessSelector()
        {
            return Status.SUCCESS;
        }

        public Status ReturnFailureSelector()
        {
            return Status.FAILURE;
        }
    }
}
