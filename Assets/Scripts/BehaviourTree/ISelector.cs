namespace BehaviourTree
{
    public interface ISelector 
    {
        public  Node.Status ReturnSuccessSelector();

        public Node.Status ReturnFailureSelector();
    }
}