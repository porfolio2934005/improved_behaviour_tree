using System;
using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using UnityEditor;

namespace BehaviourTree
{
    public class BehaviorTreeAgent : MonoBehaviour
    {
        [SerializeField] public SensorySystem _sensorySystem;

        internal readonly GoToComponent _goToComponent;

        public BehaviorTreeAgent()
        {
            _goToComponent = new GoToComponent(this);
        }

        public BehaviourTree tree;
        public NavMeshAgent agent;

        public enum ActionState { IDLE, WORKING };
        public ActionState state = ActionState.IDLE;

        
        Node.Status treeStatus = Node.Status.RUNNING;
        WaitForSeconds waitForSec;
        private Vector3 _rememberedLocation;

        public void Start()
        {
            agent = this.GetComponent<NavMeshAgent>();

            tree = new BehaviourTree();
            waitForSec = new WaitForSeconds(Random.Range(0.1f,1f));
            var cancel = Observable.FromCoroutine(CheckNodeStatus)
                .SelectMany(CheckNodeStatus)
                .Subscribe();
        }

        public Node.Status CanSee()
        {
            if (_sensorySystem._alertStage == AlertStage.Alerted)
            {
                return Node.Status.SUCCESS;
            }

            return Node.Status.FAILURE;
        }

        public Node.Status Flee(Vector3 location, float distance)
        {
            if (state == ActionState.IDLE)
            {
                _rememberedLocation = this.transform.position +
                                      (transform.position - location).normalized * distance;
            }
            return GoToLocation(_rememberedLocation);

            //return _goToComponent.GoToLocation(this.transform.position +
                                               //(transform.position - location).normalized * distance);
        }

        public Node.Status GoToLocation(Vector3 destination)
        {
            float distanceToTarget = Vector3.Distance(destination, this.transform.position);
            if (state == ActionState.IDLE)
            {
                agent.SetDestination(destination);
                state = ActionState.WORKING;
            }
            else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
            {
                state = ActionState.IDLE;
                return Node.Status.FAILURE;
            }
            else if (distanceToTarget < 2)
            {
                state = ActionState.IDLE;
                return Node.Status.SUCCESS;
            }
            return Node.Status.RUNNING;
        }

        IEnumerator CheckNodeStatus()
        {
            while (true)
            {
                treeStatus = tree.Process();
                yield return waitForSec;
            }

        }
    }
}
