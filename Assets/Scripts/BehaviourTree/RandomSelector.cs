using System.Collections.Generic;

namespace BehaviourTree
{
    public  class RandomSelector : Selector, ISelector
    {
        private Node[] nodeArray;

        private bool shuffled = false;
        public RandomSelector(string n) : base(n)
        {
            name = n;
        }
        

        public override Status Process()
        {
            if (!shuffled)
            {
                children.Shuffle();
                shuffled = true;
            }

            children.Shuffle();
            Status childstatus = children[currentChild].Process();
            if (childstatus == Status.RUNNING) return Status.RUNNING;

            if (childstatus == Status.SUCCESS)
            {
                currentChild = 0;
                shuffled = false;
                return ReturnSuccessSelector();
            }
            else
            {
                children[currentChild].sortOrder = 10;
            }

            currentChild++;
            if (currentChild >= children.Count)
            {
                currentChild = 0;
                shuffled = false;
                return ReturnFailureSelector();
            }

            return Status.RUNNING;
        }

        public Status ReturnFailureSelector()
        {
            return Status.FAILURE;
        }

        public Status ReturnSuccessSelector()
        {
            return Status.SUCCESS;
        }

    }
}
