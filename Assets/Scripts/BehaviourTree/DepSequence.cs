using UnityEngine.AI;

namespace BehaviourTree
{
    public class DepSequence : Node
    {
        private BehaviourTree _dependancy;
        private NavMeshAgent _agent;
        public DepSequence(string n, BehaviourTree d, NavMeshAgent a)
        {
            name = n;
            _dependancy = d;
            _agent = a;
        }

        public override Status Process()
        {
            if (_dependancy.Process() == Status.FAILURE)
            {
                _agent.ResetPath();
                foreach (Node n in children)
                {
                    n.Reset();
                }
                return Status.FAILURE;
            }

            Status childstatus = children[currentChild].Process();
            if (childstatus == Status.RUNNING) return Status.RUNNING;
            if (childstatus == Status.FAILURE)
                return childstatus;

            currentChild++;
            if (currentChild >= children.Count)
            {
                currentChild = 0;
                return Status.SUCCESS;
            }

            return Status.RUNNING;
        }


    }
}
