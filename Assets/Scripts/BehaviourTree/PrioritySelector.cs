using System.Collections.Generic;

namespace BehaviourTree
{
    public  class PrioritySelector : Selector, ISelector, IQuicRecursiveSort
    {
        private Node[] _nodeArray;
        private bool _orderFlag = false;

        public PrioritySelector(string n) : base(n)
        {
            
        }

        public void OrderNodes()
        {
            _nodeArray = children.ToArray();
            Sort(_nodeArray, 0, children.Count-1);
            children = new List<Node>(_nodeArray);
        }

        public override Status Process()
        {
            if (!_orderFlag)
            {
                OrderNodes();
                _orderFlag = true;
            }

            
            Status childstatus = children[currentChild].Process();
            if (childstatus == Status.RUNNING) return Status.RUNNING;

            if (childstatus == Status.SUCCESS)
            {
                children[currentChild].sortOrder = 1;
                currentChild = 0;
                _orderFlag = false;
                return ReturnSuccessSelector();
            }
            else
            {
                children[currentChild].sortOrder = 10;
            }

            currentChild++;
            if (currentChild >= children.Count)
            {
                currentChild = 0;
                _orderFlag = false;
                return ReturnFailureSelector();
            }

            return Status.RUNNING;
        }

        public Status ReturnFailureSelector()
        {
            return Status.FAILURE;
        }

        public Status ReturnSuccessSelector()
        {
            return Status.SUCCESS;
        }


        public void Sort(Node[] array, int low, int high)
        {
            if (low < high)
            {
                int partitionIndex = Partition(array, low, high);
                Sort(array, low, partitionIndex - 1);
                Sort(array, partitionIndex + 1, high);
            }
        }
        
        public int Partition(Node[] array, int low,
            int high)
        {
            Node pivot = array[high];

            int lowIndex = (low - 1);
            
            for (int j = low; j < high; j++)
            {
                if (array[j].sortOrder <= pivot.sortOrder)
                {
                    lowIndex++;

                    //TODO так как в быстрой сортировке происходит рекурсия
                    //вызов других методов происходит не в нужных местах. Это заменна набора LINQ для оптимизации.
                    //В будущем будет реализован более улучшенный и поддерживаемый метод
                    //MathOperations<Node>.Swap(array[lowIndex],array[j]);

                   

                    (array[lowIndex], array[j]) = (array[j], array[lowIndex]);
                }
            }
            


            (array[lowIndex + 1], array[high]) = (array[high], array[lowIndex + 1]);

            
            

            return lowIndex + 1;
        }
        
        
    }
}
