using UnityEngine;
using UnityEngine.AI;

namespace BehaviourTree
{
    public class RobberBehaviour : BehaviorTreeAgent
    {
        [SerializeField] private GameObject _diamond;
        [SerializeField] private GameObject _van; 
        [SerializeField] private GameObject _backdoor; 
        [SerializeField] private GameObject _frontdoor;
        [SerializeField] private GameObject _painting;
        [SerializeField] private GameObject _painting2;
        [SerializeField] private GameObject _painting3;
        [SerializeField] private GameObject _player;
        
        public GameObject[] art;
        
        
        private GameObject pickup;
        private int order;


        [Range(0, 1000)]
        public int money = 800;


        new void Start()
        {
            base.Start(); //Надо инициализировать дерево из наследуемого класса
            Leaf goToDiamond = new Leaf("Go To Diamond", GoToDiamond,2);
            Leaf goToPainting = new Leaf("Go To Painting", GoToPainting, 3);
            Leaf goToPainting2 = new Leaf("Go To Painting2", GoToPainting2, 4);
            Leaf goToPainting3 = new Leaf("Go To Painting3", GoToPainting3, 1);
            Leaf hasGotMoney = new Leaf("Has Got Money", HasMoney);
            Leaf goToBackDoor = new Leaf("Go To Backdoor", GoToBackDoor, 2);
            Leaf goToFrontDoor = new Leaf("Go To Frontdoor", GoToFrontDoor, 1);
            Leaf goToVan = new Leaf("Go To Van", GoToVan);
            PrioritySelector opendoor = new PrioritySelector("Open Door");
            Sequence runAway = new Sequence("Run Away");
            Leaf canSee = new Leaf("Can See Player?", CanSeePlayer);
            Leaf flee = new Leaf("Flee From Cop", FleeFromPlayer);
            Invertor invertMoney = new Invertor("Invert Money");
            Invertor cantSeePlayer = new Invertor("Cant See Player");
            PrioritySelector selectObject = new PrioritySelector("Select object to Steal");
               
            selectObject.AddChild(goToDiamond);
            selectObject.AddChild(goToPainting);
            selectObject.AddChild(goToPainting2);
            selectObject.AddChild(goToPainting3);
            
            invertMoney.AddChild(hasGotMoney);    
            opendoor.AddChild(goToFrontDoor);
            opendoor.AddChild(goToBackDoor);

            Sequence s1 = new Sequence("s1");
            s1.AddChild(invertMoney);
            Sequence s2 = new Sequence("s2");
            s2.AddChild(cantSeePlayer);
            s2.AddChild(opendoor);
            Sequence s3 = new Sequence("s3");
            s3.AddChild(cantSeePlayer);
            s3.AddChild(selectObject);
            Sequence s4 = new Sequence("s4");
            s4.AddChild(cantSeePlayer);
            s4.AddChild(goToVan);

            BehaviourTree seePlayer = new BehaviourTree();
            seePlayer.AddChild(cantSeePlayer);
            
            DepSequence steal = new DepSequence("Steal Something",seePlayer,agent );
            steal.AddChild(invertMoney);
            //steal.AddChild(cantSeePlayer);
            steal.AddChild(opendoor);
            //steal.AddChild(cantSeePlayer);
            steal.AddChild(selectObject);
            //steal.AddChild(cantSeePlayer);
            steal.AddChild(goToVan);

            runAway.AddChild(canSee);
            runAway.AddChild(flee);
            
            cantSeePlayer.AddChild(canSee);
            Selector beThief = new SimpleSelector("Be a thief");
            beThief.AddChild(steal);
            beThief.AddChild(runAway);
            //tree.AddChild(steal);
            tree.AddChild(beThief);
            tree.PrintTree();

        }

        public Node.Status CanSeePlayer()
        {
            return CanSee();
        }

        public Node.Status FleeFromPlayer()
        {
            return Flee(_player.transform.position, 30);
        }

        private Node.Status GoToPainting3()
        {
            if (_painting3.activeSelf == false)
                return Node.Status.FAILURE;
            Node.Status s = _goToComponent.GoToLocation(_painting3.transform.position);
            if (s == Node.Status.SUCCESS)
            {
                _painting3.transform.parent = this.gameObject.transform;
                order = Constants.PAINTING_LISA_COST;
                pickup = _painting3;
            }
            return s;
        }

        private Node.Status GoToPainting()
        {
            if (_painting.activeSelf == false)
                return Node.Status.FAILURE;
            Node.Status s = _goToComponent.GoToLocation(_painting.transform.position);
            if (s == Node.Status.SUCCESS)
            {
                _painting.transform.parent = this.gameObject.transform;
                order = Constants.PAINTING_ANOTHER_PICTURE;
                pickup = _painting;
            }
            return s;
        }
        private Node.Status GoToPainting2()
        {
            if (_painting2.activeSelf == false)
                return Node.Status.FAILURE;
            Node.Status s = _goToComponent.GoToLocation(_painting2.transform.position);
            if (s == Node.Status.SUCCESS)
            {
                _painting2.transform.parent = this.gameObject.transform;
                order = Constants.PAINTING_SOME_PICTURE;
                pickup = _painting2;
            }
            return s;
        }

        public Node.Status HasMoney()
        {
            if(money < 500)
                return Node.Status.FAILURE;
            return Node.Status.SUCCESS;
        }

        public Node.Status GoToDiamond()
        {
            if (_diamond.activeSelf == false)
                return Node.Status.FAILURE;
            Node.Status s = _goToComponent.GoToLocation(_diamond.transform.position);
            if (s == Node.Status.SUCCESS)
            {
                _diamond.transform.parent = this.gameObject.transform;
                order = Constants.DIMOND_COST;
                pickup = _diamond;
            }
            return s;
        }

        public Node.Status GoToBackDoor()
        {
            return GoToDoor(_backdoor);
        }

        public Node.Status GoToFrontDoor()
        {
            return GoToDoor(_frontdoor);
        }

        public Node.Status GoToVan()
        {
            Node.Status s = _goToComponent.GoToLocation(_van.transform.position);
            if (s == Node.Status.SUCCESS)
            {
                money += order;
                pickup.SetActive(false);
                pickup = null;
                order = 0;
            }
            return s;
        }

        public Node.Status GoToDoor(GameObject door)
        {
            Node.Status s = _goToComponent.GoToLocation(door.transform.position);
            if (s == Node.Status.SUCCESS)
            {
                if (!door.GetComponent<Lock>().isLocked)
                {
                    door.GetComponent<NavMeshObstacle>().enabled = false ;
                    return Node.Status.SUCCESS;
                }
                return Node.Status.FAILURE;
            }
            else
                return s;
        }
    }
}
