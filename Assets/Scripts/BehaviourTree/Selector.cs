namespace BehaviourTree
{
    public abstract class Selector : Node
    {
        public Selector(string n)
        {
            name = n;
        }
        public abstract override Status Process();

    }
}
