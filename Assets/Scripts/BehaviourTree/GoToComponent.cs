using UnityEngine;

namespace BehaviourTree
{
    public class GoToComponent
    {
        private BehaviorTreeAgent _behaviorTreeAgent;

        public GoToComponent(BehaviorTreeAgent behaviorTreeAgent)
        {
            _behaviorTreeAgent = behaviorTreeAgent;
        }

        public Node.Status GoToLocation(Vector3 destination)
        {
            float distanceToTarget = Vector3.Distance(destination, _behaviorTreeAgent.transform.position);
            if (_behaviorTreeAgent.state == RobberBehaviour.ActionState.IDLE)
            {
                _behaviorTreeAgent.agent.SetDestination(destination);
                _behaviorTreeAgent.state = RobberBehaviour.ActionState.WORKING;
            }
            else if (Vector3.Distance(_behaviorTreeAgent.agent.pathEndPosition, destination) >= 2)
            {
                _behaviorTreeAgent.state = RobberBehaviour.ActionState.IDLE;
                return Node.Status.FAILURE;
            }
            else if (distanceToTarget < 2)
            {
                _behaviorTreeAgent.state = RobberBehaviour.ActionState.IDLE;
                return Node.Status.SUCCESS;
            }
            return Node.Status.RUNNING;
        }
    }
}