namespace BehaviourTree
{
    public class Leaf : Node
    {
        public delegate Status Tick();
        public Tick ProcessMethod;

        public Leaf() { }

        public Leaf(string n, Tick pm)
        {
            name = n;
            ProcessMethod = pm;
        }
        
        public Leaf(string n, Tick pm, int order)
        {
            name = n;
            ProcessMethod = pm;
            sortOrder = order;
        }

        public override Status Process()
        {
            if(ProcessMethod != null)
                return ProcessMethod();
            return Status.FAILURE;
        }

    }
}
