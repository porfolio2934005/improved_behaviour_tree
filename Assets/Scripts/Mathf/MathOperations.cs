namespace BehaviourTree
{
    public static class MathOperations<T>
    {
        public static void Swap(T element1, T element2)
        {
            (element1, element2) = (element2, element1);
        }
    }
}
