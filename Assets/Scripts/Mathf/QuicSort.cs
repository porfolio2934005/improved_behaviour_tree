namespace BehaviourTree
{
    public interface QuicSort
    {
        int Partition(Node[] element, int low, int high);
    }
}