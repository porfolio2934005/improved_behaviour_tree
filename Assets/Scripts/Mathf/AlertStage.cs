public enum AlertStage
{
    Peaceful,
    Intrigued,
    Alerted
}