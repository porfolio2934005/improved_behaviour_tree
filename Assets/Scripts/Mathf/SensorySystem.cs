using UnityEngine;
using UnityEngine.Serialization;

public abstract class SensorySystem : MonoBehaviour
{
    [FormerlySerializedAs("Fov")] public float _fov;
    [FormerlySerializedAs("AlertStage")]public AlertStage _alertStage;
    [FormerlySerializedAs("AlertLevel")]
    [Range(0,100)] public  float _alertLevel;
    [FormerlySerializedAs("FowAngle")]
    [Range(0,360)] public  float _fowAngle; 
    public abstract float CalculateAngle(Collider c);
}