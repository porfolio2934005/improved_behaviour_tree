using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AgentObserve : SensorySystem
{
    private void Awake()
    {
        _alertStage = AlertStage.Peaceful;
        _alertLevel = 0;
    }

    private void Update()
    {
        bool playerInFow = false;
        Collider[] targetsInFov = Physics.OverlapSphere(
            transform.position, _fov);
        foreach (Collider c in targetsInFov)
        {
            if (c.CompareTag("Player"))
            {
                float signedAngle = CalculateAngle(c);
                if(Mathf.Abs(signedAngle) < _fowAngle/2)
                    playerInFow = true;
                break;
            }
        }

        _UpdateAlertState(playerInFow); 
    }

    public override float CalculateAngle(Collider c)
    {
        return Vector3.Angle(
            transform.forward,
            c.transform.position - transform.position);
    }

    private void _UpdateAlertState(bool playerInFow)
    {
        switch (_alertStage)
        {
            case AlertStage.Peaceful:
                if (playerInFow)
                    _alertStage = AlertStage.Intrigued;
                break;
            case AlertStage.Intrigued:
                if (playerInFow)
                {
                    _alertLevel++;
                    if (_alertLevel >= 100)
                        _alertStage = AlertStage.Alerted;
                }
                else
                {
                    _alertLevel--;
                    if (_alertLevel <= 0)
                        _alertStage = AlertStage.Peaceful;
                }
                break;
            case AlertStage.Alerted:
                if (!playerInFow)
                    _alertStage = AlertStage.Intrigued;
                break;
        }
    }
}
