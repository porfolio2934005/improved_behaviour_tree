using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
    #region Orders

        public const int DIMOND_COST = 250;
        public const int PAINTING_LISA_COST = 200;
        public const int PAINTING_SOME_PICTURE = 100;
        public const int PAINTING_ANOTHER_PICTURE = 400;


        #endregion
}
