using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AgentObserve))]
public class AgentObserveEditor : Editor
{
    private void OnSceneGUI()
    {
        AgentObserve _agent = (AgentObserve)target;
        
        Color _color = Color.blue;
        if (_agent._alertStage == AlertStage.Intrigued)
            _color = Color.Lerp(Color.green, Color.red, _agent._alertLevel / 100f);
        else if (_agent._alertStage == AlertStage.Alerted)
            _color = Color.red;
        
        Handles.color = new Color(_color.r, _color.g, _color.b, 0.3f);
        Handles.DrawSolidArc(
            _agent.transform.position,
            _agent.transform.up,
            Quaternion.AngleAxis(-_agent._fowAngle/2f, _agent.transform.up)* _agent.transform.forward,
                _agent._fowAngle,
                _agent._fov);

        Handles.color = _color;
        _agent._fov = Handles.ScaleValueHandle(
            _agent._fov,
            _agent.transform.position + _agent.transform.forward * _agent._fov,
            _agent.transform.rotation,
            3,
            Handles.SphereHandleCap,
            1);
    }
}
